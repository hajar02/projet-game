import Audio from 'ts-audio';


const card = document.querySelectorAll<HTMLElement>(".cell");
const front: any = document.querySelectorAll<HTMLElement>(".front-card");

const attempsSpan: any = document.querySelector<HTMLElement>("#attemps");
const commento: any = document.querySelector<HTMLElement>("#comment");

const button: any = document.querySelector<HTMLElement>("#button");

let star: any = document.querySelectorAll<HTMLElement>(".fa-star");

let attemps: number = 0;
let score: number = 0;

//display
const start: any = document.querySelector<HTMLElement>('.game-start');
const board: any = document.querySelector<HTMLElement>('.game-board');
const end: any = document.querySelector<HTMLElement>('.game-end');

const flipSound: any = document.querySelector<HTMLElement>('#flipcard');
const flipmp3 = Audio({
    file: String(flipSound?.src),
    loop: false,
    volume: 0.8,
    preload: true,
});

const star3Sound: any = document.querySelector<HTMLElement>('#star3sound');
const star3mp3 = Audio({
    file: String(star3Sound?.src),
    loop: false,
    volume: 0.8,
    preload: true,
});

const star2Sound: any = document.querySelector<HTMLElement>('#star2sound');
const star2mp3 = Audio({
    file: String(star2Sound?.src),
    loop: false,
    volume: 0.8,
    preload: true,
});

const star1Sound: any = document.querySelector<HTMLElement>('#star1sound');
const star1mp3 = Audio({
    file: String(star1Sound?.src),
    loop: false,
    volume: 0.8,
    preload: true,
});


shuffleCards()
clickCard()

board.style.display = 'block';
end.style.display = 'none';

function shuffleCards() {
    card.forEach(cards => {
        const num = [...Array(card.length).keys()]
        const random = Math.floor(Math.random() * card.length);
        cards.style.order = String(num[random]);
    })
}

/*function shuffleCards() {
    card.forEach((card) => {
        let randomPosition = Math.floor(Math.random() * 18);
        card.style.order = randomPosition;
    })
};
*/

function clickCard() {


    for (let i: number = 0; i < card.length; i++) {

        

        //Ora impostiamo una durata di tempo per mostrare le carte e dopo 2 secondi giriamo le carte e lasciamo l'utente giocare

        front[i].classList.add('show')
        setInterval(() => {
            front[i].classList.remove('show')

        }, 2500);

        //Cosa succedera al click

        card[i].addEventListener("click", () => {
            flipmp3.play();
            front[i].classList.add('flip')
            const flipCard = document.querySelectorAll<HTMLElement>(".flip")

            if (flipCard.length == 2) {

                checkForMatch(flipCard[0], flipCard[1]);

                attemps++; //verra aggiunto un tentativo
                attempsSpan.innerHTML = attemps;
                console.log(attemps); //per vedere se funziona

            }
        })
    }
}

function checkForMatch(cardOne: any, cardTwo: any) {

    if (cardOne.dataset.index == cardTwo.dataset.index) {

        //dataset.index perché nell'HTML ho messo data-index assegnano lo stesso numero alla coppia di carte identiche

        cardOne.classList.remove('flip'); //Annullare l'azione flip
        cardTwo.classList.remove('flip');

        cardOne.classList.add('match'); //Aggiungere l'azione match
        cardTwo.classList.add('match');

        score++;
        
        //console.log(score); //per vedere se funziona
        if (score ==12) {
        changePage();//cambio pagina all'ultima coppa di carta
        }

    } else {

        setTimeout(() => {

            //Ho messo il setTimeout perché devo vedere se le carte sono uguali in un certo periodo di tempo, 
            //se non lo avessi messo il gioco mi farebbe girare solo una carta alla volta

            cardOne.classList.remove('flip'); //Annullare l'azione flip
            cardTwo.classList.remove('flip');

        }, 800);
    }
}

function changePage() {
    if (attemps) {
        board.style.display = 'none';
        end.style.display = 'block';
        
        comment();
    }

}


function comment() {

    let starList = document.querySelectorAll<HTMLElement>('.fa-star');
    
for (const myStar of starList) {
    console.log(myStar);
    
    if (attemps>=28 && attemps<=35 ) {
        commento.innerHTML = 'WELL DONE! Try to make less than 26 attempts to earn the best score';
        myStar.classList.remove('tre');
        star2mp3.play();
       
    }
    if (attemps>=36 ) {
        commento.innerHTML = 'Nice work, but i know you can do better :)';
        myStar.classList.remove('tre');
        myStar.classList.remove('due');
        star1mp3.play();
    }

    if (attemps>=0 && attemps<=27){
            commento.innerHTML = 'YOU HAVE A VERY STRONG MEMORY!   CONGRATULATIONS CHAMPION';
            star3mp3.play();
        }
    }
}

button.addEventListener('click', () => {
    flipmp3.play();
});

Maquette:
https://www.figma.com/file/r3zNztSZz3TBBJ3taR5nJp/Untitled?node-id=0%3A1&t=8pzcRkMvtQFSsE6A-1

MEMORY-GAME

Pour mon projet de jeu j'ai choisi de faire un jeu de mémoire. Au chargement de la page, les cartes seront mélangées et mes 12 paires de cartes seront affichées. L'utilisateur aura 3 secondes pour voir les cartes et une fois le temps écoulé, il devra essayer de faire correspondre les cartes exactement. Il y a des effets sonores dans le jeu, il est donc conseillé de garder le volume sur moyen. Une fois que vous avez fini de faire correspondre les cartes, donc lorsque le score atteint 12, une autre section s'affichera indiquant le nombre d'étoiles obtenues et un commentaire basé sur le nombre de tentatives, et il y aura un bouton pour redémarrer le jeu à partir de la tête.

Bonus : Les avatars insérés dans les cartes ont été inspirés par mes camarades de classe et les formateurs :)